from django.urls import path
from projects.views import list_projects, show_project, create_project
from accounts.views import login_view

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("login/", login_view, name="login"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
